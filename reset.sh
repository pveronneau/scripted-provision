#!/bin/bash
# Verify this is run by root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
# Reset to defaults
read -p "Warning! This script resets the cloudbridge to a default state, if this is okay Press [Enter], otherwise press [CNTRL+C]"
su - osprey -c "source /home/osprey/ospreydvr/venv/bin/activate && python /home/osprey/ospreydvr/src/manage.py resetsetupparams"
