#!/bin/bash
#                         __
#                      _,'  `:--.__
#                    ,'    .       `'--._
#                  ,'    .               `-._
#          ,-'''--/    .                     `.
#        ,'      /   .    ,,-''`-._            `.
#       ; .     /   .   ,'         `,            |____
#       |      /   .   ;             :           |--._\
#       '     /   :    |      .      |           |
#        `.  ;   _     :             ;           |
#          `-:   `"     \           ,           _|==:--.__
#             \.-------._`.       ,`        _.-'     `-._ `'-._
#              \  :        `-...,-``-.    .'             `-.   | 
#               `.._         / | \     _.'                  `. | 
#                   `.._    '--'```  .'                       `|
#                       `.          /
#                .        `-.       \
#         ___   / \  __.--`/ , _,    \
#       ,',  `./,--`'---._/ = / \,    \  __
#      /    .-`           `"-/   \)_    "`
#    _.--`-<_         ,..._ /,-'` /    /
#  ,'.-.     `.    ,-'     `.    /`'.+(
# / /  /  __   . ,'    ,   `.  '    \ \ 
# |(_.'  /  \   ; |          |        ""_
# |     (   ;   `  \        /           `.
# '.     `-`   `    `.___,-`             `.
#   `.        `                           |
#    ; `-.__`                             |
#    \    -._                             |
#     `.                                  /
#      /`._                              /
#      \   `,                           /
#       `---'.     /                  ,'
#             '._,'-.              _,(_,_
#                    |`--.    ,,.-' `-.__)
#                     `--`._.'         `._)
#                                         `=-
# ____  ___ ____  _____   _____ _   _ _____   ____ ___ ____ _ 
#|  _ \|_ _|  _ \| ____| |_   _| | | | ____| |  _ \_ _/ ___| |
#| |_) || || | | |  _|     | | | |_| |  _|   | |_) | | |  _| |
#|  _ < | || |_| | |___    | | |  _  | |___  |  __/| | |_| |_|
#|_| \_\___|____/|_____|   |_| |_| |_|_____| |_|  |___\____(_)
#
# A script by Patrick Veronneau
#
# Version 1.0
#
#
# Side note: Nicole Matsui is a jerk for making me comment.
#
#### NVR prepreation script
CBCONFIGDIR=~/cb-config
echo "Enter NVR host ip:"
read NVRIP
echo "Enter the sudo password for the osprey user:"
read -s OSPREYPASS
sshpass -p $OSPREYPASS ssh-copy-id osprey@$NVRIP
# Gather information from the NVR
MINIONID=`ssh osprey@$NVRIP cat /etc/salt/minion_id`
echo "Minion ID is:"
echo "$MINIONID"
IPCONFIG=`ssh osprey@$NVRIP "ifconfig | grep HWaddr | grep -v wlan"`
echo "MAC address is:"
MACADDRESS=`echo $IPCONFIG | awk '{print $5}'`
echo $MACADDRESS
# dump facts to a file if variables have been found
if [ -n $MINIONID ] && [ -n $MACADDRESS ]; then
    mkdir -p $CBCONFIGDIR/$MINIONID
    echo "Please connect to https://portal.evostream.com and download the License.lic and place it in the $CBCONFIGDIR/$MINIONID folder"
    read -p "Press [Enter] when file has been placed into the folder"
    if grep -q $MACADDRESS $CBCONFIGDIR/$MINIONID/License.lic || grep -q $MACADDRESS $CBCONFIGDIR/$MINIONID/$MINIONID.lic; then
        echo "Correct licence found, renaming..."
        mv $CBCONFIGDIR/$MINIONID/License.lic $CBCONFIGDIR/$MINIONID/$MINIONID.lic
        echo "Dumping facts"
        echo "MINIONID=$MINIONID" > $CBCONFIGDIR/$MINIONID/facts
        echo "NVRIP=$NVRIP" >> $CBCONFIGDIR/$MINIONID/facts
        echo "MACADDRESS=$MACADDRESS" >> $CBCONFIGDIR/$MINIONID/facts
        echo "Set permissions for /home/osprey"
         ssh -t osprey@$NVRIP "echo $OSPREYPASS | sudo -S chown -R osprey:osprey /home/osprey"
         scp serial.sh activate.sh reset.sh set-static-ip.sh osprey@$NVRIP:
         ssh -t osprey@$NVRIP "echo $OSPREYPASS | sudo -S /home/osprey/serial.sh"
         ssh -t osprey@$NVRIP "rm /home/osprey/serial.sh"
        # Mount Drives
        RAWSDB1UUID="`ssh -t osprey@$NVRIP "ls -l /dev/disk/by-uuid/ | grep sdb1"`"
        SDB1UUID="`echo $RAWSDB1UUID | awk {'print $9'}`"
        RAWSDB2UUID="`ssh -t osprey@$NVRIP "ls -l /dev/disk/by-uuid/ | grep sdb2"`"
        SDB2UUID="`echo $RAWSDB2UUID | awk {'print $9'}`"
        echo "Disk mount setup:"
         ssh osprey@$NVRIP "mkdir -p /home/osprey/ospreydvr/videos"
         ssh osprey@$NVRIP "mkdir -p /home/osprey/ospreydvr/images"
        VIDEOFSTAB="`echo -e "UUID=$SDB1UUID\t/home/osprey/ospreydvr/videos\txfs\tdefaults\t0\t0"`"
        IMAGEFSTAB="`echo -e "UUID=$SDB2UUID\t/home/osprey/ospreydvr/images\txfs\tdefaults\t0\t0"`"
        echo -e "#!/bin/bash\ncat /home/osprey/disk >> /etc/fstab" > $CBCONFIGDIR/$MINIONID/catscript.sh
        echo $VIDEOFSTAB > $CBCONFIGDIR/$MINIONID/disk
        echo $IMAGEFSTAB >> $CBCONFIGDIR/$MINIONID/disk
        scp $CBCONFIGDIR/$MINIONID/disk $CBCONFIGDIR/$MINIONID/catscript.sh osprey@$NVRIP:
         ssh osprey@$NVRIP "chmod 755 /home/osprey/catscript.sh"
         ssh -t osprey@$NVRIP "echo $OSPREYPASS | sudo -S /home/osprey/catscript.sh"
         ssh -t osprey@$NVRIP "echo $OSPREYPASS | sudo -S mount -a"
         read -p "Ensure drives mounted correctly, then Press [Enter]"
        # Upload license to S3
        #aws s3 cp $CBCONFIGDIR/$MINIONID/$MINIONID.lic s3://osprey-evo-licenses/base/evo-licenses/1.6/ || exit 1 "license upload to S3 failed"
         echo "upload the license file to S3"
         echo "Connect to the salt master (10.50.33.173) and accept the key \"sudo salt-key -a $MINIONID\""
         echo "After that is complete, connect to osprey@$NVRIP and execute: sudo salt-call state.highstate --state-output=mixed"
    else
        echo "Incorrect license or missing file, abort"
    fi
else
    echo "Some variables were not found"
fi