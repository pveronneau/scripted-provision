#!/bin/bash
#                         __
#                      _,'  `:--.__
#                    ,'    .       `'--._
#                  ,'    .               `-._
#          ,-'''--/    .                     `.
#        ,'      /   .    ,,-''`-._            `.
#       ; .     /   .   ,'         `,            |____
#       |      /   .   ;             :           |--._\
#       '     /   :    |      .      |           |
#        `.  ;   _     :             ;           |
#          `-:   `"     \           ,           _|==:--.__
#             \.-------._`.       ,`        _.-'     `-._ `'-._
#              \  :        `-...,-``-.    .'             `-.   | 
#               `.._         / | \     _.'                  `. | 
#                   `.._    '--'```  .'                       `|
#                       `.          /
#                .        `-.       \
#         ___   / \  __.--`/ , _,    \
#       ,',  `./,--`'---._/ = / \,    \  __
#      /    .-`           `"-/   \)_    "`
#    _.--`-<_         ,..._ /,-'` /    /
#  ,'.-.     `.    ,-'     `.    /`'.+(
# / /  /  __   . ,'    ,   `.  '    \ \ 
# |(_.'  /  \   ; |          |        ""_
# |     (   ;   `  \        /           `.
# '.     `-`   `    `.___,-`             `.
#   `.        `                           |
#    ; `-.__`                             |
#    \    -._                             |
#     `.                                  /
#      /`._                              /
#      \   `,                           /
#       `---'.     /                  ,'
#             '._,'-.              _,(_,_
#                    |`--.    ,,.-' `-.__)
#                     `--`._.'         `._)
#                                         `=-
# ____  ___ ____  _____   _____ _   _ _____   ____ ___ ____ _ 
#|  _ \|_ _|  _ \| ____| |_   _| | | | ____| |  _ \_ _/ ___| |
#| |_) || || | | |  _|     | | | |_| |  _|   | |_) | | |  _| |
#|  _ < | || |_| | |___    | | |  _  | |___  |  __/| | |_| |_|
#|_| \_\___|____/|_____|   |_| |_| |_|_____| |_|  |___\____(_)
#
# A script by Patrick Veronneau
#
# Version 1.0
#
#
# Side note: Nicole Matsui is a jerk for making me comment.
#
#### VPN setup for the NVR systems
# Import variables from the previous script
CBCONFIGDIR=~/cb-config
echo "Input minion id:"
read NEWID
if [ ! -f $CBCONFIGDIR/$NEWID/facts ]; then
    exit 1 "Fact file not found"
fi
source $CBCONFIGDIR/$NEWID/facts
# run the initial open vpn setup
echo "Enter your administrator login name:"
read ADMINID
ssh -t $ADMINID@$NVRIP "sudo salt-call state.sls openvpn --state-output=mixed; sudo salt-call state.sls openvpn --state-output=mixed; sudo cp /opt/openvpn/easy-rsa/easyrsa3/pki/private/$MINIONID.key /opt/openvpn/certs/ && sudo cp /opt/openvpn/easy-rsa/easyrsa3/pki/reqs/$MINIONID.req ~ && sudo chown $ADMINID ~/$MINIONID.req"
scp $ADMINID@$NVRIP:$MINIONID.req $CBCONFIGDIR/$MINIONID/
echo -e "cd /home/ospreyvpn/easy-rsa/easyrsa3/\n./easyrsa import-req /tmp/$MINIONID.req $MINIONID\n./easyrsa sign-req client $MINIONID" > $CBCONFIGDIR/$MINIONID/certsign.sh
scp $CBCONFIGDIR/$MINIONID/$MINIONID.req $CBCONFIGDIR/$MINIONID/certsign.sh $ADMINID@10.50.32.76:/tmp/
ssh -t $ADMINID@10.50.32.76 "sudo chown ospreyvpn:ospreyvpn /tmp/$MINIONID.req; sudo -u ospreyvpn bash /tmp/certsign.sh && sudo rm /tmp/certsign.sh /tmp/$MINIONID.req; sudo cp /home/ospreyvpn/easy-rsa/easyrsa3/pki/issued/$MINIONID.crt ~ && sudo chown $ADMINID ~/$MINIONID.crt"
scp $ADMINID@10.50.32.76:$MINIONID.crt $CBCONFIGDIR/$MINIONID/
scp $CBCONFIGDIR/$MINIONID/$MINIONID.crt $ADMINID@$NVRIP:
ssh -t $ADMINID@$NVRIP "sudo mv $MINIONID.crt /opt/openvpn/certs/"
echo "Connect to the vpn server (10.50.32.76) and add a static client config file too the following path:"
echo "/etc/openvpn/staticclients/$MINIONID"
echo -e "It should look like\nifconfig-push 10.8.x.x 10.8.x.(x+1)\npush "route 10.50.0.0 255.255.0.0""
read -p "Press [Enter] when ready to run final vpn setup"
ssh -t $ADMINID@$NVRIP "sudo salt-call state.highstate --state-output=mixed; sudo service openvpn restart"
ssh -t $ADMINID@$NVRIP "sudo salt-call state.highstate --state-output=mixed"
echo "VPN setup complete, verify the NVR is connected using the tun0 interface."