#!/bin/bash
# Verify this is run by root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
cat > /etc/network/interfaces << EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

auto p6p1
iface p6p1 inet static
   address 192.168.1.191
   netmask 255.255.255.0
   network 192.168.1.0
   gateway 192.168.1.100
   dns-nameservers 192.168.1.100 8.8.8.8
EOF

